package br.com.rrgomide.frames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import br.com.rrgomide.utils.JCheckBoxList;

public class MainFrame extends JFrame {

  private JLabel lbCaminhoDestino;
  private JLabel lbFonteDadosAlunos;
  private JLabel lbSufixoProjeto;
  
  private JTextField txtCaminhoDestino;
  private JTextField txtFonteDadosAlunos;
  private JTextField txtSufixoProjeto;
  
  private JButton btDownloadFromGit;
  
  private JPanel pnlDados;

  private JCheckBoxList chkListAlunos;
  private DefaultListModel<JCheckBox> checkboxes;
  private JTextArea txtLog;
  
  public MainFrame() {
    
    configurarFrame();
    instanciarComponentes();
    posicionarComponentes();
    
  }

  private void configurarFrame() {
    
    this.setLayout(new FlowLayout(FlowLayout.LEFT));
    this.setSize(800, 600);
    this.setResizable(true);
    this.setLocationRelativeTo(null);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }
  
  private void instanciarComponentes() {
    
    lbCaminhoDestino = new JLabel("Pasta de download:");
    lbFonteDadosAlunos = new JLabel("Lista de alunos:");
    lbSufixoProjeto = new JLabel("Sufixo do projeto:");
   
    txtCaminhoDestino = new JTextField(System.getProperty("user.dir"), 55);
    txtFonteDadosAlunos = new JTextField("usuarios_git.txt", 55);
    txtSufixoProjeto = new JTextField("", 55);
    
    pnlDados = new JPanel(new BorderLayout(5, 5));
    pnlDados.setBorder(BorderFactory.createLineBorder(Color.BLACK));
    
    checkboxes = new DefaultListModel<>();
    
    List<String> usuarios = new ArrayList<>();
    String fileName = System.getProperty("user.dir") + "\\usuarios.txt";
    
    //Abrindo e lendo arquivo usuarios.txt
    try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
      stream.forEach(line -> checkboxes.addElement(new JCheckBox(line, true)));
    } 
    catch (IOException e) {
      e.printStackTrace();
    }
    
    chkListAlunos = new JCheckBoxList(checkboxes);
    chkListAlunos.setSize(200, 400);
    
    txtLog = new JTextArea(16, 48);
  }
  
  private void posicionarComponentes() {
    
    this.add(lbCaminhoDestino);
    this.add(txtCaminhoDestino);
    
    //this.add(lbFonteDadosAlunos);
    //this.add(txtFonteDadosAlunos);
    
    this.add(lbSufixoProjeto);
    this.add(txtSufixoProjeto);
    
    pnlDados.add(chkListAlunos, BorderLayout.LINE_START);
    pnlDados.add(txtLog, BorderLayout.CENTER);
    this.add(pnlDados);
  }
}
