package br.com.rrgomide.test;

import javax.swing.JFrame;

import br.com.rrgomide.frames.MainFrame;

public class Main {

  public static void main(String[] args) {

    JFrame mainFrame = new MainFrame();
    mainFrame.setVisible(true);
  }
}
